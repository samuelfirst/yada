#!/bin/bash -i

echo 'Removing /usr/bin/yada'
sudo rm /usr/bin/yada

echo 'Removing ~/.config/yadarc'
rm ~/.config/yadarc.py

echo 'Removing Root Node'
rm -r ~/.local/share/yada

echo 'Removing /usr/share/bash-completion/yada'
sudo rm /usr/share/bash-completion/yada

echo 'Removing help/yada alias'
sed -i "/alias help=yada/d" ~/.bashrc
