# YADA

Yet Another Documentation Application (alternatively, YADA's a Documentation 
Application)

![](assets/demo.webm)

## Index

* [What Is YADA?](https://gitlab.com/samuelfirst/yada#what-is-yada)
  * [Project Goals](https://gitlab.com/samuelfirst/yada#project-goals)
  * [How it's Different From Other Programs](https://gitlab.com/samuelfirst/yada#how-its-different-from-other-programs)
	* [Man](https://gitlab.com/samuelfirst/yada#man)
	* [Info](https://gitlab.com/samuelfirst/yada#info)
* [How It Works](https://gitlab.com/samuelfirst/yada#how-it-works)
  * [Abstract](https://gitlab.com/samuelfirst/yada#abstract)
  * [Detailed](https://gitlab.com/samuelfirst/yada#detailed)
  * [Files](https://gitlab.com/samuelfirst/yada#files)
* [Installation](https://gitlab.com/samuelfirst/yada#installation)
  * [Dependencies](https://gitlab.com/samuelfirst/yada#dependencies)
  * [Install](https://gitlab.com/samuelfirst/yada#install)
	* [Python >= 3.6](https://gitlab.com/samuelfirst/yada#python-36)
	* [Python < 3.6](https://gitlab.com/samuelfirst/yada#python-36-1)
  * [Uninstall](https://gitlab.com/samuelfirst/yada#uninstall)
* [Extending YADA](https://gitlab.com/samuelfirst/yada#extending-yada)
  * [Using Html](https://gitlab.com/samuelfirst/yada#using-html)
  * [Using Markdown](https://gitlab.com/samuelfirst/yada#using-markdown)
  * [Pulling Documentation From Cheat.sh](https://gitlab.com/samuelfirst/yada#pulling-documentation-from-cheatsh)
* [Run Control](https://gitlab.com/samuelfirst/yada#run-control)
* [License](https://gitlab.com/samuelfirst/yada#license)

## What is YADA?

YADA is a program for viewing and editing documentation.

### Project Goals

This project aims to provide a discoverable interace for
browsing and creating documentation.  This means that
rather than providing documentation with the program, it
gives the user the ability to quickly and easily create
their own custom documentation (you could think of it as 
a specialized note-taking application).  The goal is not 
to replace programs like man or info, but to supplement
them with the user's own notes and thoughts.

Discoverability is obtained by storing the documentation
in a hierarchichal database.  This allows the user to
store documentation files under categories and
sub-categories.

Creating/editing and viewing documentation is handled by the
default editor/pager accordingly.

### How it's Different From Other Programs

Programs like man and info provide similar functionality
to YADA, however, there are a few key differences with
both the design goals, and the way they are implemented.

#### Man

The goal of man is to provide information on installed
packages created by the package maintainers.  This differs
from the goal of YADA, which is to allow the user to
record their own documentation and notes.

While man does shell out to the default pager, it does not
provide an interface for the user to edit the manpage or
provide an easily editable format (it uses troff).

Man's interface is entirely cli based, rather than tui
based, like YADA's.  This means that while it is faster
to navigate if you know what you're looking for, it is
less discoverable.

#### Info

The goal of info is to provide a more modern approach
to the problem solved by man.  Rather than using troff,
it uses a hypertext format.  This allows it to link
various documents together, but makes editing them
slightly more difficult than editing plain text.  It 
does not provide a way to open the file in an editor from 
within the program.

Info does not shell out to the default pager, opting
instead to roll its own solution.  This raises the 
complexity of the program, and forces the user to learn 
to use a new tool.

Info provides a tui interface similar to YADA, but it
does not provide color-coding, which makes it more
difficult to distinguish what is and is not a link.
It also does not allow the user to remap the default
keybindings.

## How it Works

### Abstract

The program provides a hierarchichal database of nodes and 
documentation files that can be traversed with `WASD`, `HJKL`, or
the `arrow keys`.

Opening a documentation file will open it with the default
pager.  Opening a node will drop down into that node's level
of the database.

Files can be edited with the default editor, by pressing either
the `y` or `c` keys.

To exit the program, press `q` or `C-c`

### Detailed

The program uses the file system as a database.  Nodes are
actually directories, and documentation files are
plain text files.

Each new level of the is entered recursively.  This has a
few advantages:

* The code is a bit cleaner, and more compact

* States of previous directories are remembered
without needing to be recorded in a seperate variable.

There are also disadvantages:

* The default limit for recursion in python is 1000.

This means that without changing the value of the recursionLimit
variable in the run control file, the program will crash after
going down 1000 levels.  This is more of an issue in theory than
in practice, and is unlikely to impact most (or any) users.

The program provides neither a pager, nor editor, instead shelling
out to whatever is the default on the system.  This allows the
user to use tools that they are familiar with, rather than forcing
them to learn new ones.  It also has the benefit of simplifying the
program, which not only makes it lighter, but also easier to debug.

### Files

The project contains several files to supplement the main program.

The table below contains the name of each file and its purpose.

| File         | Purpose                                            |
|:-------------|:---------------------------------------------------|
| yada.py      | The program                                        |
| yadarc.py    | The program's run control file                     |
| yada         | Bash completions                                   |
| install.sh   | Installs the program                               |
| uninstall.sh | Uninstalls the program                             |
| todo.org     | Roadmap/todo-list                                  |
| README.md    | Provides information/documentation for the program |
| COPYING      | The program's license (GPLv3)                      |


## Installation

### Dependencies

* Some form of unix/unix-like (MacOsX, Linux, BSD, etc.)
* Python 3.6 or greater
  * If you are running an older version of python, there is a seperate set of 
  install instructions below.
* ncurses

### Install

#### Python >= 3.6

* Change the value of `editor` in yadarc to your preferred
text editor.

* `./install.sh`

#### Python < 3.6

* Change the value of `editor` in yadarc to your preferred
text editor

* `pip install f2format`
  * [f2format](https://github.com/JarryShaw/f2format#f2format)

* `f2format yada.py`

* `./install.sh`

### Uninstall

To uninstall, run `./uninstall.sh`

## Extending YADA

Because YADA is decoupled from the pager, it can be hacked to use formats
other than plain text.

### Using Html

To use html for your documentation, simply switch the pager specified in the
run control file to the web browser of your choice (I recommend w3m).

### Using Markdown

To use markdown for your documentation, first install pandoc and w3m, then
save the below script as yada-markdown-pager.sh, and make it executable with
`chmod +x`.  Finally, change the pager specified in the run control file to
the path to the script.

```bash
#!/bin/bash
pandoc "$1" | w3m -T text/html
```

### Pulling Documentation From Cheat.sh

If you want to use documentation that others have written, rather than writing
your own, you can use the documentation provided by the cheat.sh project.  The
[cheat sheet sources](https://github.com/chubin/cheat.sh#cheat-sheets-sources)
are in markdown, so this should pair well with the method above to use markdown.

## Run Control

The run control file contains variables that, as
the name implies, control how the program runs.  It
is located either in ~/.config/yadarc.py, ./.yadarc.py,
or anywhere specified by the -l flag.

The table below contains the names and descriptions
of the variables.

| Variable        | Description                                                     |
|:----------------|:----------------------------------------------------------------|
| root            | The default location of the root node                           |
| verbose         | Whether or not output should be verbose                         |
| fileColorFG     | The default foreground color to use when displaying files       |
| fileColorBG     | The default background color to use when displaying files       |
| dirColorFG      | The default foreground color to use when displaying directories |
| dirColorBG      | The default background color to use when displaying directories |
| selectColorFG   | The default foreground color to use for the selected item       |
| selectColorBG   | The default background color to use for the selected item       |
| foregroundColor | The default foreground color to use for generic text            |
| backgroundColor | The default background color to use for generic text            |
| editor          | The default text editor to shell out to                         |
| pager           | The default pager to shell out to                               |
| prevDir         | The key(s) to jump back to the previous node                    |
| nextDir         | The key(s) to jump to the next node                             |
| downList        | The key(s) to move down the list of items                       |
| upList          | The key(s) to move up the list of items                         |
| open            | The key(s) to open an item                                      |
| toggleLog       | The key(s) to toggle logging on or off                          |
| newDir          | The key(s) to create a new node                                 |
| newFile         | The key(s) to create a new documenation file                    |
| editFile        | The key(s) to edit an existing documentation file               |
| delete          | The key(s) to delete a node/file                                |
| move            | The key(s) to move a node/file                                  |
| quit            | The key(s) to exit the program                                  |
| recursionLimit  | The default limit for recursion                                 |

If you want to change the value of the default colors, you
can use the script below to cycle through the possible colors.
To run it, save it to a file, and run it with the command
`python3 [file]`.


To move to the next color, press any key.

```python
import curses
def x(stdscr):
    curses.start_color()
    curses.use_default_colors()
    for i in range(255):
		curses.init_pair(1,i,0)
		stdscr.clear()
		stdscr.addstr(0,0,f'{i}:xyzzy', curses.color_pair(1))
		stdscr.refresh()
		stdscr.getch()
    stdscr.getch()
curses.wrapper(x)
```

## License

YADA is licensed under the GNU GPLv3, meaning you are free to
copy/modify/distribute all or part of it, so long as you
provide both a copy of the source and a copy of the GPL with
it or any derivative works.

More information can be found in COPYING
