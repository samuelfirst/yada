#!/usr/bin/python3
'''
YADA
Copyright (C) 2018 Samuel First

    YADA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    YADA is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YADA.  If not, see <https://www.gnu.org/licenses/>.
'''

import argparse
import os
import shutil
import sys
import curses

# Displays list of files in current directory
def mainDisplay(stdscr, currentDirectory, files, selected, status):
    """Display for main part of interface"""
    # Set up colors and clear screen
    curses.start_color()
    curses.use_default_colors()
    curses.init_pair(1, config.selectColorFG, config.selectColorBG)
    curses.init_pair(2, config.fileColorFG, config.fileColorBG)
    curses.init_pair(3, config.dirColorFG, config.dirColorBG)
    curses.init_pair(4, config.foregroundColor, config.backgroundColor)
    curses.curs_set(0)
    paintBackground(stdscr)

    # Display list of files
    colPos = 0
    for item in files:
        # Shorten Item Name to Avoid Rolling Over
        # or, if it's shorter than the max length:
        # pad it out with spaces.
        newItem = f'{item}{" "*curses.COLS}'[:curses.COLS - 1]

        # Display File Names in Their Assosciated Colors
        if item == selected:
            stdscr.addstr(colPos, 0, f'*{newItem}', curses.color_pair(1))
        elif os.path.isfile(f'{currentDirectory}/{item}'):
            stdscr.addstr(colPos, 0, f' {newItem}', curses.color_pair(2))
        else:
            stdscr.addstr(colPos, 0, f' {newItem}', curses.color_pair(3))
        colPos += 1

    # Attempt to display status/command help on last three lines
    try:
        stdscr.addstr(curses.LINES - 3, 0,
                      f'{status}',
                      curses.color_pair(4))
        stdscr.addstr(curses.LINES - 2, 0,
                      f'{config.prevDir[0]} prev node    ' +
                      f'{config.upList[0]} select up      ' +
                      f'{config.open[0]} open      ' +
                      f'{config.newDir[0]} new node    ' +
                      f'{config.quit[0]} quit',
                      curses.color_pair(4))
        stdscr.addstr(curses.LINES - 1, 0,
                      f'{config.nextDir[0]} next node    '+
                      f'{config.downList[0]} select down    '+
                      f'{config.delete[0]} delete    '+
                      f'{config.newFile[0]} new file    ' +
                      f'{config.editFile[0]} edit file',
                      curses.color_pair(4))
    except:
        pass
        
    stdscr.refresh()


# Set each line to the default background color pair (4)
def paintBackground(stdscr):
    """Make background default color"""
    stdscr.clear()
    for i in range(curses.LINES):
        stdscr.addstr(i, 0, ' '*(curses.COLS - 1), curses.color_pair(4))


# Partially disable curses so other programs
# don't get messed up when shelling out
def disableCurses():
    """Partially Disables Curses"""
    curses.echo()
    curses.nocbreak()
    curses.endwin()

# Start curses back up after
def enableCurses():
    """Re-enable Curses"""
    curses.noecho()
    curses.cbreak()

# Displays interface for getting file/directory names 
def getName(stdscr, fileOrDirectory):
    """Get name of file or directory"""
    # Prompt for and get name
    paintBackground(stdscr)
    stdscr.addstr(curses.LINES//2, 0,
                  f' {fileOrDirectory} name:',
                  curses.color_pair(4))
    stdscr.refresh()
    curses.echo()
    name = stdscr.getstr(curses.LINES // 2,
                         len(fileOrDirectory) + 7)
    curses.noecho()
    return name


# Give yes/no prompt, return true for yes, false for no
def promptYN(stdscr, prompt):
    """Prompt user y/n"""
    while True:
        # Prompt for and get y/n
        paintBackground(stdscr)
        stdscr.addstr(curses.LINES//2, 0,
                      f'{prompt}[y/n]',
                      curses.color_pair(4))
        stdscr.refresh()
        key = str(stdscr.getkey())
        if key == 'y':
            return True
        elif key == 'n':
            return False


# Try to open file w/ config.editor, $EDITOR, $VISUAL, ed
# in that order
def editFile(fileName):
    """Shell out to editor to edit file"""
    # Escape quotes/escapes to stop code injection
    fileName = fileName.replace('\\','\\\\').replace('"','\\"')
    disableCurses()
    
    try:
        os.system(f'{config.editor} "{fileName}"')
    except AttributeError:
        if os.getenv('EDITOR') != None:
            os.system(f'$EDITOR "{fileName}"')
        elif os.getenv('VISUAL') != None:
            os.system(f'$VISUAL "{fileName}"')
        else:
            os.system(f'touch "{fileName}" ; ed "{fileName}"')

    enableCurses()


# Attempt to open file with config.pager, then $PAGER,
# then, if all else fails, cat and read
def pageFile(fileName):
    """Shell out to pager to page file"""
    fileName = fileName.replace('\\','\\\\').replace('"','\\"')
    disableCurses()
    
    try:
        os.system(f'{config.pager} "{fileName}"')
    except AttributeError:
        if os.getenv('PAGER') != None:
            os.system(f'$PAGER "{fileName}"')
        else:
            os.system(f'cat "{fileName}"; read -n 1')

    enableCurses()


# Determine if dir or file; copy accordingly
def copyItem(fromItem, toItem):
    """Copy dir/file"""
    if os.path.isfile(fromItem):
        shutil.copy(fromItem, toItem)
    else:
        shutil.copytree(fromItem, toItem)

# Works like copyItem, but deletes
def removeItem(item):
    """Delete dir/file"""
    if os.path.isfile(item):
        os.remove(item)
    else:
        shutil.rmtree(item)
    

# 1. Draw the screen
# 2. Get keystrokes
# 3. Check keystrokes against bindings defined
#    in the run control file
# 4. If there is a match:
#  4a. Perform the assosciated action
# 5. Update log if logging is enabled
def main(stdscr, root, log, movePath, moveItem):
    """Main interface of the program"""
    itemsPerScreen = curses.LINES - 3 # -3 corrects for help-text offset
    currentPosition = 0  # The current position of the first item
    selection = 0        # The position of the currently selected item
    statusLine = ''      # Message to display in status line
    
    while True:
        files = os.listdir(root)
        files.sort()
        adjustedPosition = selection + currentPosition
        
        if len(files) != 0:
            try:
                displayFiles = files[currentPosition:currentPosition +
                                 itemsPerScreen]
                mainDisplay(stdscr, root, displayFiles,
                            files[adjustedPosition], statusLine)
                currentItem = f'{root}/{files[adjustedPosition]}'

            # If there's an index error, reset position to first item
            except IndexError:
                selection = 0
                currentPosition = 0
                currentItem = f'{root}/files[0]'
                mainDisplay(stdscr, root, displayFiles, files[0], statusLine)
        else:
            mainDisplay(stdscr, root, files, '', statusLine)
            currentItem=''
            
        # The reason this isn't just a multi-line f-string is
        # because python gets really finnicky about indentation.
        if args.verbose:
            log.append('VARIABLES:\n' +
                       f'\titemsPerScreen: {itemsPerScreen}\n' +
                       f'\tcurrentPosition: {currentPosition}\n' +
                       f'\tselection: {selection}\n' +
                       f'\tadjustedPosition: {adjustedPosition}\n' +
                       f'\tcurrentItem: {currentItem}\n')

        # Get keystroke
        key = str(stdscr.getkey())
        if args.verbose:
            log.append(f'KEY_PRESS: {key}')

        # Go back to previous directory and return movePath/moveItem
        if key in config.prevDir and root != args.root:
            if args.verbose:
                log.append('ACTION: prevDir')
            return(movePath, moveItem)

        # If a directory is selected, go to it.
        # If a file is selected, shell out to $PAGER
        elif key in config.nextDir or key in config.open:
            if args.verbose:
                log.append('ACTION: nextDir/open')

            if os.path.isfile(currentItem):
                pageFile(currentItem)
                stdscr.keypad(True)
            elif currentItem != '':
                movePath, moveItem = main(stdscr,
                                          currentItem,
                                          log,
                                          movePath,
                                          moveItem)
            statusLine = ''

        # Move down one item in the list
        # If it's the last item, loop around to the first
        # If it's the last item on the screen, jump to next page
        elif key in config.downList:
            if args.verbose:
                log.append('ACTION: downList')
            if currentItem != '':
                if files[adjustedPosition] == files[-1]:
                    currentPosition = 0
                    selection = 0
                elif selection == itemsPerScreen - 1:
                    currentPosition += itemsPerScreen
                    selection = 0
                else:
                    selection += 1

        # Move up one item in the list
        # If it's the first item, loop around to the last
        # If it's at the top of the screen, jump to previous page
        elif key in config.upList:
            if args.verbose:
                log.append('ACTION: upList')
            if currentItem != '':
                if adjustedPosition == 0:
                    currentPosition = len(files) - itemsPerScreen
                    selection = itemsPerScreen - 1
                elif selection == 0:
                    # When scrolling up to the first page,
                    # currentPosition - itemsPerScreen will
                    # be negative, which will cause problems
                    # when we pass a slice of files to displayMain.
                    if currentPosition - itemsPerScreen <= 0:
                        selection = currentPosition - 1
                        currentPosition = 0
                    else:
                        currentPosition -= itemsPerScreen
                        selection = itemsPerScreen - 1
                else:
                    selection -= 1

        # Toggle logging on or off
        elif key in config.toggleLog:
            if args.verbose:
                log.append('ACTION: toggleLog')
            if args.verbose:
                args.verbose = False
                statusLine = 'Logging: Disabled'
            else:
                args.verbose = True
                statusLine = 'Logging: Enabled'

        # Create new sub-directory in current directory
        elif key in config.newDir:
            if args.verbose:
                log.append('ACTION: newDir')
            newDirName = getName(stdscr, 'directory').decode('utf-8')
            os.mkdir(f'{root}/{newDirName}')
            statusLine = f'New Node: {newDirName}'

        # Shell out to editor to create new file
        elif key in config.newFile:
            if args.verbose:
                log.append('ACTION: newFile')
            newFileName = getName(stdscr, 'file').decode('utf-8')
            editFile(f'{root}/{newFileName}')
            stdscr.keypad(True)
            statusLine = f'New File: {newFileName}'

        # Shell out to editor to edit existing file
        elif key in config.editFile:
            if args.verbose:
                log.append('ACTION: editFile')
            if currentItem != '':
                if os.path.isfile(currentItem):
                    editFile(currentItem)
                    stdscr.keypad(True)
                    statusLine = f'Edited File: {files[adjustedPosition]}'

        # Prompt to delete file/dir; if yes: delete
        elif key in config.delete:
            if args.verbose:
                log.append('ACTION: delete')
            if currentItem != '':
                if promptYN(stdscr, f'Delete {files[adjustedPosition]}?'):
                    removeItem(currentItem)
                    statusLine = f'Deleted: {files[adjustedPosition]}'
                    
                    if selection > 0:
                        selection -= 1

        # If there is not file in movePath:
        #  Set movePath as the path to selection
        #  Set moveItem to files[adjustedPosition]
        # If there is a file in movePath:
        #  Move the file to the current directory
        elif key in config.move:
            if movePath == '':
                movePath = currentItem
                moveItem = files[adjustedPosition]
                statusLine = f'Preparing to Move: {moveItem}'
            else:
                if movePath != currentItem:
                    copyItem(movePath, f'{root}/{moveItem}')
                    removeItem(movePath)
                    
                movePath = ''
                statusLine = f'Moved: {moveItem}'

        # Exit the program
        elif key in config.quit:
            if args.verbose:
                log.append('ACTION: quit')
            exit()


# Only runs if script is not imported
if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-l',
                        '--load',
                        type=str,
                        help='Load alternate run control file',
                        default=None)
    parser.add_argument('-r',
                        '--root',
                        type=str,
                        help='Load alternate root node',
                        default=None)
    # If this flag is enabled, all messages will be
    # logged and dumped as the program exits.
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true')
    parser.add_argument('-o',
                        '--out',
                        type=str,
                        default=None)

    args = parser.parse_args()

    
    # Load run control file
    # If any args are equal to None:
    #  Overload them with args from rc file.
    sys.dont_write_bytecode = True
    if args.load == None:
        sys.path.insert(0, os.path.expanduser('~/.config'))
        rcFile = 'yadarc'
    else:
        # Split args.load into (path, file)
        runControl = os.path.split(args.load)
        sys.path.insert(0, runControl[0])
        rcFile = os.pathsplitext(runControl[1])[0]
    try:
        config = __import__(rcFile)
    except:
        print('Could not load run control file')
        exit()

    if args.root == None:
        args.root = config.root
    if not args.verbose:
        try:
            args.verbose = config.verbose
        except AttributeError:
            pass

    try:
        sys.setrecursionlimit(config.recursionLimit)
    except AttributeError:
        pass


    # Call main in curses wrapper
    # Dump log when program exits.
    log = []
    args.root = os.path.expanduser(args.root)
    try:
        curses.wrapper(main, args.root, log, "", "")
    except KeyboardInterrupt:
        pass
    except Exception as error:
        print(error, '\n', '='*50, '\n')
    finally:
        if args.out == None:
            for message in log:
                print(message)
        else:
            with open(args.out, 'a') as outFile:
                for message in log:
                    outFile.write(f'{message}\n')
