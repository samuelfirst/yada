#!/bin/bash -i

echo 'Copying yada.py to /usr/bin'
chmod +x yada.py
sudo cp yada.py /usr/bin/yada

echo 'Copying yadarc.py to ~/.config'
cp -n yadarc.py ~/.config

echo 'Creating root in ~/.local/share/yada'
mkdir -p ~/.local/share/yada/root

echo 'Copying yada to /usr/share/bash-completion'
sudo cp yada /usr/share/bash-completion

read -n 1 -sp 'Alias help to yada?[Y/n]' response
case "$response" in
     n|N)
	 echo -e '\nhelp not aliased to yada.'
	 ;;
     *)
	 echo 'alias help=yada' >> ~/.bashrc
	 ;;
esac
