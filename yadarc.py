root='~/.local/share/yada/root'
verbose=False
fileColorFG=7
fileColorBG=0
dirColorFG=12
dirColorBG=0
selectColorFG=11
selectColorBG=0
backgroundColor=0
foregroundColor=7
editor='emacs -nw'
pager='less'

# Keybindings
prevDir=['h','a','KEY_LEFT']
nextDir=['l','d','KEY_RIGHT']
downList=['j','s','KEY_DOWN']
upList=['k','w','KEY_UP']
open=['o','e']
toggleLog=['`']
newDir=['b','r']
newFile=['n','f']
editFile=['y','c']
delete=['p','z']
move=['t']
quit=['q']

# DO NOT change the value of
# this unless you know what
# you are doing.  Changing it
# can lead to unexpected
# behavior.
recursionLimit=1000
